# Programme Matinée 4

[Groupes séparés, puis plénière]

## Finalisation de l’analyse statistique
- Les participants mettent la dernière main à leurs analyses (améliorer les graphiques, les tables, présenter les résultats, ...).
- Rappel sur les bonnes pratiques pour une recherche reproductible
- Export des scripts et des résultats des groupes sous forme d’un document computationnel et/ou d’une trame d’article de recherche grâce à l’utilisation de templates (e.g., rticles package).

## Mise en commun des connaissances
Restitution orale par les stagiaires sur la base d’une trame fournie en début de formation : un.e volontaire de chaque langage présente sa solution et l’export « propre » de ses analyses. Chaque restitution permet des échanges avec la salle.
