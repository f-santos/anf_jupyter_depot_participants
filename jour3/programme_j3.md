# Programme Matinée 3

[En groupes séparés]

## Analyse des données
- Tous les groupes font du versionnage et mettent en œuvre d’autres bonnes pratiques logicielles 
- Des rappels vers le guide pratique « Traçabilité des données de la recherche » (RQeR) seront faits tout du long de l’analyse de données.
- On ajoute une touche de calcul de variable issue de la théorie de l'information (intercorrélation).
- On visualise les différentes variables et on applique des tests statitiques.
