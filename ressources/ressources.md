Ressources additionnelles
=========================

Nous listons ci-dessous un certain nombre de ressources utiles, aussi bien en amont qu'en aval de la formation.

## Recherche reproductible

## Jupyter
* Le [JupyterLab tour](https://github.com/jupyterlab-contrib/jupyterlab-tour) donne, pour les nouveaux venus dans l'environnement Jupyter, un petit tutoriel de bienvenue assez utile. 
  Pour l'installer, taper simplement :
  ```
  conda install -c conda-forge jupyterlab-tour
  ```
  dans une console Anaconda. Les infobulles du tutoriel seront automatiquement ouvertes lors de la prochaine exécution de JupyterLab.

## R

## Python
