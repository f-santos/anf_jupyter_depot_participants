## Import des packages requis :
library(groundhog)                           # favorise la reproductibilité
today <- "2021-08-01"                        # date de version des packages à suivre
groundhog.library(beeswarm, date = today)    # représentation graphique
groundhog.library(data.table, date = today)  # chargement rapide des gros fichiers
groundhog.library(fs, date = today)          # outils divers pour gérer des fichiers
groundhog.library(here, date = today)        # gestion des chemins dans un projet
groundhog.library(nbconvertR, date = today)  # conversion de notebooks ipynb -> R
groundhog.library(repr, date = today)        # gestion des graphiques R sous Jupyter
groundhog.library(RJSONIO, date = today)     # import de fichiers .json

## Un exemple d'utilisation de here :
here()

## Construire le chemin vers le sous-répertoire "data" :
here("data")

## Construire le chemin vers le sous-répertoire "data/ALL_DATA/" :
here("data", "ALL_DATA")

## Exportation du notebook en script R :
## nbconvert(
##     file = here("jour2", "jour2_R", "01_workflow_jupyter.ipynb"),
##     fmt = "script" # format choisi pour l'export : script R
## )
