#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 29 07:25:40 2021

@author: chalgand
"""

import os
import json
import numpy as np
import pandas as pd

DATA_PATH = os.path.join('..', '..', 'data', 'ALL_DATA')
ALL_SUJETS = os.listdir(DATA_PATH)

DATA_STATIC_DICT = {'name' : list(),
                    'force_level' : list(),
                    'num_essai': list(),
                    'ctrl' : list(),
                    'phase' : list(),
                    'cutoff' : list(),
                    'tracking': list(),
                    'percent_err': list()}

DATA_TEMPO = None

def homogénéisation_data_tempo(data_tempo):
    routine_time = (data_tempo['routine_time'].values -
                    data_tempo['routine_time'].values[0])
    routine_time = routine_time * 1000
    routine_time = np.around(routine_time)
    routine_time = routine_time.astype('int32')
    data_tempo.index = pd.to_datetime(routine_time,
                                      origin=pd.Timestamp('2021-07-01'),
                                      unit='ms')
    data_tempo_res = data_tempo.asfreq(freq='1ms')
    data_tempo_res = data_tempo_res.interpolate(method='linear')
    data_tempo_hom = data_tempo_res.asfreq(freq='20ms')
    data_tempo_hom = data_tempo_hom.drop(columns=['routine_time'])
    return data_tempo_hom

def append_to_data_static(DATA_STATIC_DICT,
                          name, force_level,
                          num_essai, ctrl,
                          phase, cutoff,
                          poursuite,
                          percent_err):
    DATA_STATIC_DICT['name'].append(name)
    DATA_STATIC_DICT['force_level'].append(force_level)
    DATA_STATIC_DICT['num_essai'].append(num_essai)
    DATA_STATIC_DICT['ctrl'].append(ctrl)
    DATA_STATIC_DICT['phase'].append(phase)
    DATA_STATIC_DICT['cutoff'].append(cutoff)
    DATA_STATIC_DICT['tracking'].append(poursuite)
    DATA_STATIC_DICT['percent_err'].append(percent_err)
    return DATA_STATIC_DICT

for root, _, files in os.walk(DATA_PATH):
    if 'sujet' in root:
        print(root)
        print(files)
        for un_essai in files:
            filename, _ = os.path.splitext(un_essai)
            (name, ctrl, phase, cutoff,
             poursuite, num_essai) = filename.split('_')
            with open(os.path.join(root, un_essai), 'r') as fnm:
                contenu = json.load(fnm)

            # gestion du dictionnaire DATA_STATIC
            try:
                force_level = contenu['force_level']
            except KeyError:
                force_level = np.nan
            DATA_STATIC_DICT = append_to_data_static(DATA_STATIC_DICT,
                                                     name, force_level,
                                                     num_essai, ctrl,
                                                     phase, cutoff,
                                                     poursuite,
                                                     contenu['percent_err'])

            # gestion des données temporelles
            data_tempo_dict = {'routine_time' : contenu['routine_time'],
                               f'pos_ct_{filename}' : contenu['pos_ct'],
                               f'reco_data_{filename}' : contenu['reco_data'],
                               f'fb_err_{filename}' : contenu['fb_err']}

            data_tempo_raw = pd.DataFrame(data_tempo_dict)
            data_tempo_hom = homogénéisation_data_tempo(data_tempo_raw)
            if DATA_TEMPO is None:
                DATA_TEMPO = data_tempo_hom
            else:
                DATA_TEMPO = pd.concat([DATA_TEMPO, data_tempo_hom], axis=1)


DATA_STATIC = pd.DataFrame(DATA_STATIC_DICT)

# =============================================================================
# Save dataframe
# =============================================================================
compression_opts = dict(method='zip',
                        archive_name='data_static.csv')
DATA_STATIC.to_csv('data_static.zip',
                   compression=compression_opts)

compression_opts = dict(method='zip',
                        archive_name='data_tempo.csv')
DATA_TEMPO.to_csv('data_tempo.zip',
                   compression=compression_opts)
