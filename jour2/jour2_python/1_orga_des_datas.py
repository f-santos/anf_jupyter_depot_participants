#!/usr/bin/env python
# coding: utf-8

# # Exploitons l'organisation des données
# 
# Les données se trouvent dans le dossier _data/ALL_DATA_

# In[1]:


get_ipython().system('ls data/ALL_DATA')


# En informatique l'exécution d'un code se fait à partir d'un chemin de l'arborescence de notre OS nommé _Current Work Directory_
# 
# Quel est le nôtre actuellement ?

# In[2]:


import os
os.getcwd()


# Il est toujours intéressant de travailler en relatif car le code ne se retrouvera pas modifié si le dossier principal est déplacé au sein du PC ou sur un autre PC.
# 
# Pour remonter dans l'arborescence lorsque nous spécifions un chemin, nous devons utiliser la notion des `.`

# In[3]:


get_ipython().system('ls ../../data/ALL_DATA')


# In[4]:


ALL_SUJETS = get_ipython().getoutput('ls ../../data/ALL_DATA')
len(ALL_SUJETS)


# Nous savons maintenant que le dossier _ALL_DATA_ dispose de 22 dossiers qui correspondent chacun à l'enregistrement des données d'un sujet.
# 
# Regardons maintenant le contenu de ces répertoires. Prenons deux dossiers au hasard.

# In[5]:


get_ipython().system('ls ../../data/ALL_DATA/sujet3')


# In[6]:


get_ipython().system('ls ../../data/ALL_DATA/sujet8')


# In[7]:


SUJET3_FILES = get_ipython().getoutput('ls ../../data/ALL_DATA/sujet3')
len(SUJET3_FILES)


# In[8]:


SUJET8_FILES = get_ipython().getoutput('ls ../../data/ALL_DATA/sujet8')
len(SUJET8_FILES)


# Chaque dossier de sujet dispose de 20 fichiers json pour lesquels nous accédons à travers le titre à plusieurs informations :
# 
# - _nom du sujet_
# - _type de contrôle_
# - _phase : entrainement ou test_
# - _fréquence de coupure_
# - _type de la poursuite_
# - _numéro de l'essai_

# Allons plus loin en regardant le contenu d'un fichier

# In[9]:


import json


# In[10]:


with open('../../data/ALL_DATA/sujet8/sujet8_vit_ent_0.4_pe_10.json', 'r') as fnm:
    contenu = json.load(fnm)


# In[11]:


contenu.keys()


# Chaque fichier est pourvu d'un certain nombre d'informations :
# 
# - _pv_cond_ : contrôle position ou vitesse
# - _fb_err_ : erreur au cours du mouvement entre curseur et cible à suivre
# - _pos_ct_ : positions de la cible à suivre en pratique
# - _percent_err_ : erreur sur la trajectoire
# - _routine_time_ : timestamp au courrs de l'expérience
# - _reco_data_ : données reconstruites depuis le capteur de force en pixels
# - _duree_essai_ : durée de l'essai
# - _force_level_ : force du sujet

# In[12]:


contenu['pv_cond']


# In[13]:


contenu['fb_err']


# In[14]:


contenu['pos_ct']


# In[15]:


contenu['percent_err']


# In[16]:


contenu['routine_time']


# In[17]:


contenu['reco_data']


# In[18]:


contenu['duree_essai']


# In[19]:


contenu['force_level']


# L'étape suivante consiste à construire deux dataframes à partir de l'ensemble des données. Nous allons poursuivre en format script.

# In[20]:


get_ipython().system('jupyter nbconvert --to script 1_orga_des_datas.ipynb')

