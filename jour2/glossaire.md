# Glossaire

Son but est de rendre homogène l'utilisation des noms sur l'ensemble des deux parcours.

## Données d'origine

**sujetX** : nom de dossier de chaque sujet. X est un chiffre pour différencier les sujets.

**sujetX_ctrl_phase_freq_poursuite_numeroEssai.json** : nom des fichiers dans chaque dossier.

**ctrl** : `pos` ou `vit` : type de contrôle utilisé (contrôle position ou contrôle vitesse)  
**phase** : `ent` ou `test` : étape dans le protocole (phase d'entrainement ou phase de test)  
**freq** : `0.2`, `0.3`, `0.4` ou `0.6` : (Hz) fréquence de coupure du bruit blanc gaussien  
**poursuite** : `pc` ou `pe` : type de la tâche (tracking - pc : poursuite de cible ou fbtracking - pe : poursuite d'erreur du suivi de la cible)
**numeroEssai** : numéro de l'essai lors de la passation de l'expérience

**instant_t** : (s) instants auxquels les données sont présentées (_pos_cible_) et acquises (_reco_data_, réponse au précédent _pos_cible_) (_routine_time_)  
**pos_cible** : (pixel) position de la cible à suivre (_pos_ct_)  
**pos_sujet** : (pixel) position du curseur contrôlé par le sujet (_reco_data_)     
**pos_err** : (pixel) différence entre pos_sujet et pos_cible (_fb_err_)

## Nom de variables 

**data_path** : chemin des données (_data_path_)  
**subject_path** : chemin du dossier d'un sujet (_root_)  
**subject_files** : liste de l'ensemble des fichiers d'enregistrement d'un sujet (_files_)  
**trial** : nom d'un fichier d'enregistrement d'un essai (_un_essai_)  
**filename** : racine du fichier (_filename_)  
**data** : contenu du fichier (_contenu_)  

**data_tempo_dict** : dictionnaire pour création de Dataframe des données temporelles (data_tempo_dict)  
**data_tempo_raw** : dataframe temporel brut (_data_tempo_raw_)  
**data_tempo_res** : dataframe temporel resampling à 1ms (_data_tempo_res_)  
**data_tempo_hom** : dataframe temporel homogénéisé (_data_tempo_hom_)
**data_tempo** : dataframe temporel de l'ensemble de l'expérience (_data_tempo_)  

**data_analytic** : dataframe d'analyse contenant l'ensemble de l'expérience (_data_static_)
